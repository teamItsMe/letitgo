package com.itsme.letitgo.company.info.model.mapper;

import java.util.List;

import com.itsme.letitgo.company.info.model.dto.CompanyTestDTO;

public interface CompanyTestMapper {

	List<CompanyTestDTO> selectedInfoCompany();

}
