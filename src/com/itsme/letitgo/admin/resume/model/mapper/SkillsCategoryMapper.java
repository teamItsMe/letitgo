package com.itsme.letitgo.admin.resume.model.mapper;

import com.itsme.letitgo.admin.resume.model.dto.SkillsCategoryDTO;

public interface SkillsCategoryMapper {

	int skillsCategoryInsert(SkillsCategoryDTO requestSkillsCategory);

}
