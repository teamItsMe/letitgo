package com.itsme.letitgo.personal.info.model.mapper;

import com.itsme.letitgo.personal.info.model.dto.MemberDTO;

public interface MemberMapper {

	MemberDTO selectMemberInfo();

}
