<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/admin.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/animate.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/bootstrap.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/font-awesome.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/fonts.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/reset.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/owl.theme.default.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/flaticon.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/style.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/style_II.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/responsive.css" />
</head>
<body>
	<div class="v1686_231">
		<div class="v1686_232"></div>
		<div class="v1686_233">
			<span class="v1686_234">게시물 관리</span>
		</div>
		<div class="v1686_235">
			<span class="v1686_236">회원 탈퇴 사유</span> 
			<span class="v1686_237">회원 탈퇴</span>
		</div>
		<div class="v1686_238">
			<span class="v1686_239">공고 수정 요청</span> 
			<span class="v1686_240">공고 등록 요청</span> 
			<span class="v1686_241">공고 관리</span>
		</div>
		<div class="v1686_242">
			<span class="v1686_243">결제 내역 관리</span> 
			<span class="v1686_244">상품 관리</span> 
			<span class="v1686_245">결제 관리</span>
		</div>
		<div class="v1686_246">
			<span class="v1686_247">기업 정보 변경 요청</span> 
			<span class="v1686_248">1:1 문의</span> 
			<span class="v1686_249">회원 관리</span> 
			<span class="v1686_250">기업 회원 가입 요청</span>
		</div>
		<div class="v1686_251">
			
		</div>
		
	</div>
</body>
</html>