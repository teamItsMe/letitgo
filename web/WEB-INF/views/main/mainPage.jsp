<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Job Pro Responsive HTML Template</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="description" content="Job Pro" />
<meta name="keywords" content="Job Pro" />
<meta name="author" content="" />
<meta name="MobileOptimized" content="320" />
<!--srart theme style -->
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/animate.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/bootstrap.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/font-awesome.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/fonts.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/reset.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/owl.carousel.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/owl.theme.default.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/flaticon.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/style.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/style_II.css" />
<link rel="stylesheet" type="text/css"
	href="${ pageContext.servletContext.contextPath }/resources/css/responsive.css" />
<!-- favicon links -->
<link rel="shortcut icon" type="image/png"
	href="${ pageContext.servletContext.contextPath }/resources/image/header/favicon.ico" />
</head>
<body>

	<!-- jp first sidebar Wrapper Start -->
	<div class="jp_first_sidebar_main_wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="width: 100%;">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="cc_featured_product_main_wrapper">
								<div
									class="jp_hiring_heading_wrapper jp_job_post_heading_wrapper">
									<h2>지금 당장 보세요! </h2>
								</div>
							</div>
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane fade in active" id="best">
									<div class="ss_featured_products">
										<div class="owl-carousel owl-theme">
											<div class="item" data-hash="zero">
												<div class="jp_job_post_main_wrapper_cont">
													<div class="jp_job_post_main_wrapper">
														<div class="row">
															<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
																<div class="jp_job_post_side_img">
																	<img src="${ pageContext.servletContext.contextPath }/resources/image/content/job_post_img1.jpg" alt="post_img" />
																</div>
																<div class="jp_job_post_right_cont">
																	<h4>HTML Developer (1 - 2 Yrs Exp.)</h4>
																	<p>Webstrot Technology Pvt. Ltd.</p>
																	<ul>
																		<li><i class="fa fa-cc-paypal"></i>&nbsp; $12K -
																			15k P.A.</li>
																		<li><i class="fa fa-map-marker"></i>&nbsp;
																	</ul>
																			Caliphonia, PO 455001</li>
																</div>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
																<div class="jp_job_post_right_btn_wrapper">
																	<ul>
																		<li><a href="#"><i class="fa fa-heart-o"></i></a></li>
																		<li><a href="#">Part Time</a></li>
																		<li><a href="#">Apply</a></li>
																	</ul>
																</div>
															</div>
														</div>
													</div>
													<div class="jp_job_post_keyword_wrapper">
														<ul>
															<li><i class="fa fa-tags"></i>Keywords :</li>
															<li><a href="#">ui designer,</a></li>
															<li><a href="#">developer,</a></li>
															<li><a href="#">senior</a></li>
															<li><a href="#">it company,</a></li>
															<li><a href="#">design,</a></li>
															<li><a href="#">call center</a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="jp_register_section_main_wrapper">
							<div class="jp_regis_left_side_box_wrapper">
								<div class="jp_regis_left_side_box">
									<img
										src="${ pageContext.servletContext.contextPath }/resources/image/content/regis_icon.png"
										alt="icon" />
									<h4>I’m an EMPLOYER</h4>
									<p>
										Signed in companies are able to post new<br> job offers,
										searching for candidate...
									</p>
									<ul>
										<li><a href="#"><i class="fa fa-plus-circle"></i>
												&nbsp;REGISTER AS COMPANY</a></li>
									</ul>
								</div>
							</div>
							<div class="jp_regis_right_side_box_wrapper">
								<div class="jp_regis_right_img_overlay"></div>
								<div class="jp_regis_right_side_box">
									<img
										src="${ pageContext.servletContext.contextPath }/resources/image/content/regis_icon2.png"
										alt="icon" />
									<h4>I’m an candidate</h4>
									<p>
										Signed in companies are able to post new<br> job offers,
										searching for candidate...
									</p>
									<ul>
										<li><a href="#"><i class="fa fa-plus-circle"></i>
												&nbsp;REGISTER AS COMPANY</a></li>
									</ul>
								</div>
								<div class="jp_regis_center_tag_wrapper">
									<p>OR</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- jp first sidebar Wrapper End -->

	<!--main js file start-->
	<script
		src="${ pageContext.servletContext.contextPath }/resources/js/jquery_min.js"></script>
	<script
		src="${ pageContext.servletContext.contextPath }/resources/js/bootstrap.js"></script>
	<script
		src="${ pageContext.servletContext.contextPath }/resources/js/jquery.menu-aim.js"></script>
	<script
		src="${ pageContext.servletContext.contextPath }/resources/js/jquery.countTo.js"></script>
	<script
		src="${ pageContext.servletContext.contextPath }/resources/js/jquery.inview.min.js"></script>
	<script
		src="${ pageContext.servletContext.contextPath }/resources/js/owl.carousel.js"></script>
	<script
		src="${ pageContext.servletContext.contextPath }/resources/js/modernizr.js"></script>
	<script
		src="${ pageContext.servletContext.contextPath }/resources/js/custom.js"></script>
	<!--main js file end-->
	
	<script src="${ pageContext.servletContext.contextPath }/resources/js/donggi/select.js"></script>
</body>
</html>